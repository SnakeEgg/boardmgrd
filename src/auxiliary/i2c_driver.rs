use stm32f3xx_hal as hal;
use hal::i2c::{Instance, SclPin, SdaPin};
use hal::rcc::{self, Clocks};
use hal::time::rate::*;
use heapless::Vec;

pub struct I2cDriver<I2C, SDA, SCL> {
    i2c: I2C,
    _sda: SDA,
    _scl: SCL
}

impl<I2C, SDA, SCL> I2cDriver<I2C, SDA, SCL> 
{
    pub fn new (i2c: I2C,
                sda: SDA,
                scl: SCL,
                freq: Hertz,
                clocks: Clocks,
                bus: &mut <I2C as rcc::RccBus>::Bus,
    ) -> Result<Self, ()>
    where
        I2C: Instance,
        SDA: SdaPin<I2C>,
        SCL: SclPin<I2C>,
    {
        let freqi = freq.integer();

        if freqi > 1_000_000 {
            return Err(())
        }

        I2C::enable(bus);
        I2C::reset(bus);

        let i2cclock = I2C::clock(&clocks).0;
        let ratio = i2cclock / freqi - 4;

        let (presc, scldel, sdadel, sclh, scll) = if freq <= 100_000.Hz() {
            // Standard mode
            let presc = ratio / 514;
            let sdadel = i2cclock / 2_000_000 / (presc + 1);
            let scldel = i2cclock / 800_000 / (presc + 1) - 1;
            let sclh = ((ratio / (presc + 1)) - 2) / 2;
            let scll = sclh;

            (presc, scldel, sdadel, sclh, scll)
        } else if freq <= 400_000.Hz() {
            // Fast mode
            let presc = ratio / 387;
            let sdadel = i2cclock / 8_000_000 / (presc + 1);
            let scldel = i2cclock / 2_000_000 / (presc + 1) - 1;
            let sclh = ((ratio / (presc + 1)) - 3) / 3;
            let scll = 2 * (sclh + 1) - 1;

            (presc, scldel, sdadel, sclh, scll)
        } else {
            // Fast mode plus
            let presc = ratio / 387;
            let sdadel = 0;
            let scldel = i2cclock / 4_000_000 / (presc + 1) - 1;
            let sclh = ((ratio / (presc + 1)) - 3) / 3;
            let scll = 2 * (sclh + 1) - 1;

            (presc, scldel, sdadel, sclh, scll)
        };

        if presc >= 16 || scldel >= 16 || sdadel >= 16 {
            return Err(())
        }

        i2c.timingr.write(|w| {
            w.presc().bits(presc as u8);
            w.scldel().bits(scldel as u8);
            w.sdadel().bits(sdadel as u8);
            w.sclh().bits(sclh as u8);
            w.scll().bits(scll as u8)
        });

        i2c.cr1.write(|w| w.pe().set_bit());
        Ok(Self { i2c, _sda:sda, _scl:scl })
    }
}

impl<I2C, SDA, SCL> I2cDriver<I2C, SDA, SCL>
where
    I2C: Instance,
{
    pub fn read(&self, start_addr: u16, sub_addr: u8, bytes: u8) -> Result<Vec<u8, 256>, ()>
    {
        let cr2 = &self.i2c.cr2;
        let isr = &self.i2c.isr;
        let txdr = &self.i2c.txdr;
        let rxdr = &self.i2c.rxdr;

        cr2.write(|w| {
            w.start().set_bit();
            w.sadd().bits(start_addr);
            w.rd_wrn().clear_bit();
            w.nbytes().bits(1);
            w.autoend().clear_bit()
        });

        while isr.read().txis().bit_is_clear() {}
        txdr.write(|w| w.txdata().bits(sub_addr));

        while isr.read().tc().bit_is_clear() {}
        cr2.modify(|_,w| {
            w.start().set_bit();
            w.nbytes().bits(bytes);
            w.rd_wrn().set_bit();
            w.autoend().set_bit()
        });

        let mut read_bytes: Vec<u8, 256> = Vec::new();
        for _ in 0..bytes {
            while isr.read().rxne().bit_is_clear() {}
            if let Err(_) = read_bytes.push(rxdr.read().rxdata().bits())
            {
                return Err(())
            }
        }

        Ok(read_bytes)
    }

    pub fn read_byte(&self, start_addr: u16, sub_addr: u8) -> Result<u8, ()>
    {
        match self.read(start_addr, sub_addr, 1) {
            Ok(vec) => Ok(vec[0]),
            Err(e) => Err(e),
        }
    }

    pub fn write(&self, start_addr: u16, sub_addr: u8, bytes: &[u8]) -> Result<(), ()>
    {
        let cr2 = &self.i2c.cr2;
        let isr = &self.i2c.isr;
        let txdr = &self.i2c.txdr;
        let len = bytes.len();

        cr2.write(|w| {
            w.start().set_bit();
            w.sadd().bits(start_addr);
            w.rd_wrn().clear_bit();
            w.nbytes().bits((len+1) as u8);
            w.autoend().set_bit()
        });

        while isr.read().txis().bit_is_clear() {}
        txdr.write(|w| w.txdata().bits(sub_addr));

        for &byte in bytes {
            while isr.read().txis().bit_is_clear() {}
            txdr.write(|w| w.txdata().bits(byte));
        }

        Ok(())
    }

    pub fn write_byte(&self, start_addr: u16, sub_addr: u8, byte: u8) -> Result<(),()>
    {
        self.write(start_addr, sub_addr, &[byte])
    }
}
