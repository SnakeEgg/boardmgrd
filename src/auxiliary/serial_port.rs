use stm32f3xx_hal::pac::usart1::RegisterBlock as usart1_Regs;
use core::fmt::{self, Write};
use heapless::Vec;

pub struct SerialPort {
    usart1: &'static usart1_Regs
}

impl SerialPort {
    pub fn new(usart1: &'static usart1_Regs) -> SerialPort {
        SerialPort { usart1: usart1 }
    }

    pub fn read(&self) -> Vec<u8, 128> {
        let mut buff: Vec<u8, 128> = Vec::new();
        loop {
            // TODO If I use protobuf for this, stopping on specific bytes might
            // not work
            let byte = self.read_byte();

            if byte == 10 || byte == 13
            {
                break buff
            }
            else if let Err(_) = buff.push(byte)
            {
                break buff
            }
        }
    }

    pub fn read_byte(&self) -> u8 {
        while self.usart1.isr.read().rxne().bit_is_clear() {}
        self.usart1.rdr.read().rdr().bits() as u8
    }
}

impl Write for SerialPort {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for &b in s.as_bytes() {
            while self.usart1.isr.read().txe().bit_is_clear() {}
            self.usart1.tdr.write(|w| w.tdr().bits(b as u16));
        }
        Ok(())
    }
}

macro_rules! uprint {
    ($serial:expr, $($arg:tt)*) => {
        $serial.write_fmt(format_args!($($arg)*)).ok()
    };
}

macro_rules! uprintln {
    ($serial:expr, $fmt:expr) => {
        uprint!($serial, concat!($fmt, "\n\r"))
    };
    ($serial:expr, $fmt:expr, $($arg:tt)*) => {
        uprint!($serial, concat!($fmt, "\n\r"), $($arg)*)
    };
}
