#[macro_use]
pub mod serial_port;
pub mod lsm303agr;
pub mod gyroscope;

pub mod i2c_driver;
use i2c_driver::I2cDriver;

use stm32f3xx_hal::{
    spi::{self, Spi},
    prelude::*,
    pac::{self, USART1},
    serial::Serial,
};

pub fn init () -> (lsm303agr::Lsm303agr, serial_port::SerialPort, gyroscope::Gyroscope) 
{
    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let mut gpioa = dp.GPIOA.split(&mut rcc.ahb);
    let mut gpiob = dp.GPIOB.split(&mut rcc.ahb);
    let mut gpioc = dp.GPIOC.split(&mut rcc.ahb);
    let mut gpioe = dp.GPIOE.split(&mut rcc.ahb);

    let scl = gpiob.pb6.into_af_open_drain(
        &mut gpiob.moder,
        &mut gpiob.otyper,
        &mut gpiob.afrl
    );
    let sda = gpiob.pb7.into_af_open_drain(
        &mut gpiob.moder,
        &mut gpiob.otyper,
        &mut gpiob.afrl
    );
    let i2c1_driver = I2cDriver::new(
        dp.I2C1, 
        sda, 
        scl, 
        200_000.Hz(), 
        clocks, 
        &mut rcc.apb1
    ).unwrap();
    let lsm303agr_device = lsm303agr::Lsm303agr::new(i2c1_driver);

    let tx = gpioc.pc4.into_af_push_pull(
        &mut gpioc.moder,
        &mut gpioc.otyper,
        &mut gpioc.afrl
    );
    let rx = gpioc.pc5.into_af_push_pull(
        &mut gpioc.moder,
        &mut gpioc.otyper,
        &mut gpioc.afrl
    );
    Serial::new(dp.USART1, (tx, rx), 115_200.Bd(), clocks, &mut rcc.apb2);
    let serial_port_device = unsafe { serial_port::SerialPort::new(&*USART1::ptr()) };

    let sck = gpioa.pa5.into_af_push_pull(
        &mut gpioa.moder,
        &mut gpioa.otyper,
        &mut gpioa.afrl
    );
    let miso = gpioa.pa6.into_af_push_pull(
        &mut gpioa.moder,
        &mut gpioa.otyper,
        &mut gpioa.afrl
    );
    let mosi = gpioa.pa7.into_af_push_pull(
        &mut gpioa.moder,
        &mut gpioa.otyper,
        &mut gpioa.afrl
    );
    let cs = gpioe.pe3.into_push_pull_output(
        &mut gpioe.moder,
        &mut gpioe.otyper
    );
    let _mode = spi::Mode {
        polarity:spi::Polarity::IdleHigh,
        phase:spi::Phase::CaptureOnSecondTransition
    };
    let spi = Spi::<_,_,u8>::new(dp.SPI1, (sck, miso, mosi), 1.MHz(), clocks, &mut rcc.apb2);
    let gyroscope_device = gyroscope::Gyroscope::new(spi, cs);


    (lsm303agr_device, serial_port_device, gyroscope_device)
}
