pub extern crate stm32f3xx_hal as hal;
use hal::spi::Spi;
use hal::gpio::*;
use hal::pac::SPI1;
use hal::prelude::*;
use heapless::Vec;

type SPI = Spi<SPI1, (PA5<AF5<PushPull>>, PA6<AF5<PushPull>>, PA7<AF5<PushPull>>)>;
type CS = PE3<Output<PushPull>>;

pub struct Gyroscope {
    spi: SPI,
    cs: CS
}

impl Gyroscope{
    fn read_spi (&mut self, addr: u8, bytes: u8) -> Vec<u8,128>
    {
        let mut msg = [0u8; 128];
        msg[0] = 0b1100_0000 | addr;

        self.cs.set_low().ok();
        let out = self.spi.transfer(&mut msg).unwrap();
        self.cs.set_high().ok();

        Vec::from_slice(&out[1..((bytes+1) as usize)]).unwrap()
    }

    fn write_spi (&mut self, addr: u8, byte: u8)
    {
        let mut msg: [u8; 2] = [addr, byte];
        self.cs.set_low().ok();
        self.spi.transfer(&mut msg).unwrap();
        self.cs.set_high().ok();
    }

    fn combine_bytes(h: u8, l: u8) -> i16
    {
        let h = (h as u16) << 8;
        let l = l as u16;
        (h + l) as i16
    }

    pub fn new (spi: SPI, cs: CS) -> Self
    {
        let mut gyro = Gyroscope { spi: spi, cs: cs };
        gyro.write_spi(0x20, 0b0000_1111);

        gyro
    }

    #[allow(dead_code)]
    pub fn whoami(&mut self) -> u8
    {
        self.read_spi(0x0F, 1)[0]
    }

    pub fn gyroscope(&mut self) -> [i16;3]
    {
        self.read_spi(0x25,1);
        let xyz = self.read_spi(0x28,6);
        let x = Self::combine_bytes(xyz[1], xyz[0]);
        let y = Self::combine_bytes(xyz[3], xyz[2]);
        let z = Self::combine_bytes(xyz[5], xyz[4]);
        [x,y,z]
    }
}
