use crate::auxiliary::i2c_driver::I2cDriver;
use stm32f3xx_hal::pac::I2C1;
use stm32f3xx_hal::gpio::*;

const ACCELEROMETER: u16 = 0b0011_0010;
const OUT_TEMP_L_A: u8 = 0x0C;
const TEMP_CFG_REG_A: u8 = 0x1F;
const CTRL_REG1_A: u8 = 0x20;
const OUT_X_L_A: u8 = 0x28;

const MAGNETOMETER: u16 = 0b0011_1100;
const OFFSET_X_REG_L_M: u8 = 0x45;
const CFG_REG_A_M: u8 = 0x60;
const OUTX_L_REG_M: u8 = 0x68;

type I2C = I2cDriver<I2C1, PB7<AF4<OpenDrain>>, PB6<AF4<OpenDrain>>>;

pub struct Lsm303agr {
    i2c: I2C,
    offset_x: i16,
    offset_y: i16,
    offset_z: i16
}

impl Lsm303agr
{
    fn combine_bytes(h:u8, l:u8) -> i16
    {
        let h = h as u16;
        let l = l as u16;
        ((h << 8) + l) as i16
    }

    pub fn new(i2c: I2C) -> Self
    {
        i2c.write_byte(ACCELEROMETER, CTRL_REG1_A, 0b0101_0111).ok();
        i2c.write_byte(ACCELEROMETER, TEMP_CFG_REG_A, 0b1100_0000).ok();
        i2c.write_byte(ACCELEROMETER, CTRL_REG1_A+3, 0b1000_1000).ok();
        i2c.write_byte(MAGNETOMETER, CFG_REG_A_M, 0).ok();

        let xyz = i2c.read(MAGNETOMETER, OFFSET_X_REG_L_M, 6).unwrap();
        let x = Self::combine_bytes(xyz[1], xyz[0]);
        let y = Self::combine_bytes(xyz[3], xyz[2]);
        let z = Self::combine_bytes(xyz[5], xyz[4]);

        Lsm303agr { i2c, offset_x:x, offset_y:y, offset_z:z }
    }

    pub fn temp(&self) -> i8
    {
        // Working
        let hl = self.i2c.read(ACCELEROMETER, 0x80 | OUT_TEMP_L_A, 2).unwrap();
        hl[1] as i8 + 25
    }

    pub fn magnetometer(&self) -> [i16;3]
    {
        // Working
        let xyz = self.i2c.read(MAGNETOMETER, OUTX_L_REG_M, 6).unwrap();
        let x = Self::combine_bytes(xyz[1], xyz[0]) + self.offset_x;
        let y = Self::combine_bytes(xyz[3], xyz[2]) + self.offset_y;
        let z = Self::combine_bytes(xyz[5], xyz[4]) + self.offset_z;

        [x, y, z]
    }

    pub fn accelerometer(&self) -> [f32;3]
    {
        // Working
        let xyz = self.i2c.read(ACCELEROMETER, 0x80 | OUT_X_L_A, 6).unwrap();
        let x = Self::combine_bytes(xyz[1], xyz[0]) >> 4;
        let y = Self::combine_bytes(xyz[3], xyz[2]) >> 4;
        let z = Self::combine_bytes(xyz[5], xyz[4]) >> 4;

        let x = (x as f32) / 1000.0;
        let y = (y as f32) / 1000.0;
        let z = (z as f32) / 1000.0;

        [x, y, z]
    }
}
