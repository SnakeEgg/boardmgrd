#![no_main]
#![no_std]

extern crate panic_itm;
use cortex_m_rt::entry;
use core::fmt::Write;
use core::str::from_utf8;

#[macro_use]
mod auxiliary;

#[entry]
fn main() -> ! {
    let (lsm303agr, mut serial, mut gyroscope) = auxiliary::init();

    loop {
        uprint!(serial, "Enter a number: ");
        let byte = serial.read_byte();
        uprintln!(serial, "");

        match byte as char {
            '1' => {
                uprintln!(serial, "Led enabling not implemented");
            },
            '2' => {
                uprintln!(serial, "Led disabling not implemented");
            },
            '3' => {
                uprintln!(serial, "Led toggling not implemented");
            },
            '4' => {
                uprint!(serial, "Enter string: ");
                let s = serial.read();
                let s = from_utf8(&s).unwrap();
                uprintln!(serial, "");
                uprintln!(serial, "{}", s);
            },
            '5' => {
                uprintln!(serial, "{:?}", lsm303agr.temp());
            },
            '6' => {
                uprintln!(serial, "{:?}", lsm303agr.magnetometer());
            },
            '7' => {
                uprintln!(serial, "{:?}", lsm303agr.accelerometer());
            },
            '8' => {
                uprintln!(serial, "{:?}", gyroscope.gyroscope());
            },
            _ => {
                uprintln!(serial, "Bad input");
            },
        }
    }
}
